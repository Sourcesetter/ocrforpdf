## Preconditions
# x-code needs to be installed

# PDF rendering library open source
brew install poppler

# Install Google tesseract OCR-lib.
brew install tesseract

#  Installation of necessary libraries
python3 -mpip install -r requirements.txt --user

## run with
python3.9 main.py -i inputfile.pdf -o outputfile.pdf

# resulution 150x150 Pixel
python3.9 main.py -i inputfile.pdf -o outputfile.pdf -q 150

