#!/usr/bin/python3

from os import 	walk,\
	        makedirs,\
		devnull
from os.path import join,\
		    exists,\
		    dirname,\
		    abspath,\
		    basename
import wget
import ssl
from sys import exit, stdout
from time import strftime, gmtime
from argparse import ArgumentParser
from annot import index_pdf

def parse_args():
	p = ArgumentParser()

	p.add_argument(	"-i", "--ifile", dest = "ifile",
					metavar = "",
					type = abspath, default = None,
					help = "Input file path.")

	p.add_argument(	"-o", "--ofile", dest = "ofile",
					metavar = "",
					type = abspath, default = None,
					help = "Output file path.")

	p.add_argument(	"-d", "--dir", dest = "idir",
					metavar = "",
					type = abspath, default = None,
					help = "Input directory.")

	p.add_argument(	"-n", "--odir", dest = "odir",
					metavar = "",
					type = abspath, default = None,
					help = "Output directory.")
	p.add_argument(	"-r", dest = "replace",
					action = "store_true",
					help = "Annotated files replace original ones.")
	p.add_argument(	"-c", "--cores", dest = "cores",
					default = 1, type = int,
					metavar = "",
					help = "Number of cores/threads to use.")
	p.add_argument(	"-q", "--quality", dest = "dpi",
					default = 100, type = int,
					metavar = "",
					help = "Image quality used for detection/saving."\
					" High quality leads to large output files. Default 100")
	args = p.parse_args()

	if args.replace and args.ifile is not None:
		args.ofile = args.ifile
	elif args.replace and args.idir is not None:
		args.odir = args.idir

	files_given = args.ifile is not None and args.ofile is not None
	dir_given = args.idir is not None and args.odir is not None

	try:

		if files_given == dir_given:
			print("Supply either filenames or directories.")
			raise ValueError()

		elif args.ifile is not None and args.ofile is None:
			print("Missing ofile for ifile.")
			raise ValueError()

		elif args.ofile is not None and args.ifile is None:
			print("Missing ifile for ofile.")
			raise ValueError()

		elif args.odir is not None and args.idir is None:
			print("Missing idir for odir.")
			raise ValueError()

		elif args.idir is not None and args.odir is None:
			print("Missing idir for odir.")
			raise ValueError()

		elif files_given:
			if not exists(args.ifile):
				print("ifile: {} not found.".format(args.ifile))
				raise ValueError()

		elif dir_given:
			err = False
			if not exists(args.idir):
				print("idir: {} not found.".format(args.idir))
				raise ValueError()

	except:
		p.print_help(); exit(0)

	return args

def _yield_files(idir, odir):

	for root, _, files in walk(idir):
		for file in files:
			ifile = join(root, file)
			ofile = ifile.replace(idir, odir)
			yield ifile, ofile


if __name__ == "__main__":

	args = parse_args()

	model_url = "https://github.com/tesseract-ocr/tessdata_best/raw/"\
	"master/deu.traineddata"

	script_dir = dirname(abspath(__file__))
	tesseract_profile = join(script_dir, basename(model_url))

	if not exists(tesseract_profile):
		print("Tesseract model missing. Downloading model from %s" % model_url)
		ssl._create_default_https_context = ssl._create_unverified_context
		wget.download(model_url)


	io = zip([args.ifile], [args.ofile])
	n_files = 1
	dir_given = args.idir is not None and args.odir is not None
	if dir_given:
		io = _yield_files(args.idir, args.odir)
		n_files = len(list(_yield_files(args.idir, args.odir)))

	index = 0
	for ifile, ofile in io:
		stdout.write("{}\t{}/{}\t{:30s}\t\t".format(
			strftime("%d/%m/%Y %H:%M:%S", gmtime()),
			index +1,
			n_files,
			basename(ifile)
			))
		stdout.flush()

		ofile_dirname = dirname(ofile)
		if not exists(ofile_dirname):
			makedirs(ofile_dirname, exist_ok = True)
		try:
			index_pdf(ifile, ofile, ncores = args.cores, dpi = args.dpi)
			stdout.write("Success\n")
		except Exception as e:
			stdout.write("Error\n");print(e)
		index += 1
