from io import BytesIO
from pytesseract import image_to_pdf_or_hocr
from concurrent.futures import ThreadPoolExecutor
from pdf2image import convert_from_path, convert_from_bytes
from PyPDF2 import PdfFileWriter, PdfFileReader

def _ocr_pdf(ifile, page = None, dpi = 100, nice = 5, config = ""):

	if page is not None:
		page_count = [page]
	else:
		page_count = PdfFileReader(ifile).getNumPages()
		page_count = range(page_count)

	output = PdfFileWriter()
	for i in page_count:
		page_image_array = convert_from_path(
			ifile,
			dpi = dpi,
			first_page = i + 1,
			last_page = i + 1
			)
		ocr_page = image_to_pdf_or_hocr(
			page_image_array[0],
			extension = 'pdf',
			nice = nice,
			config = config
		)
		output.addPage(PdfFileReader(BytesIO(ocr_page)).getPage(0))

	out_obj = BytesIO()
	output.write(out_obj)
	return out_obj.getvalue()

def index_pdf(ifile, ofile, ncores = 1, nice = 5, dpi = 100,
 	tesseract_config = '--oem 1 -l deu -c preserve_interword_spaces=1 textonly_pdf=1'):

	output = PdfFileWriter()
	page_count = PdfFileReader(ifile).getNumPages()

	if ncores > 1:
		with ThreadPoolExecutor(max_workers=ncores) as executor:
			pages = executor.map(
				lambda p: _ocr_pdf(
					ifile = ifile,
					page = p,
					dpi = dpi,
					nice = nice,
					config = tesseract_config
					)
				,range(page_count))
			for n_page in pages:
				output.addPage(PdfFileReader(BytesIO(n_page)).getPage(0))
	else:
		pages = _ocr_pdf(
			ifile = ifile,
 			dpi = dpi,
			nice = nice,
			config = tesseract_config,
			)
		output.appendPagesFromReader(PdfFileReader(BytesIO(pages)))


	ostream = open(ofile, "wb")
	output.write(ostream)
	ostream.close()
